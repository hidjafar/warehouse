class Product < ApplicationRecord
  validates :brand, presence: true
  validates :code, presence: true
  validates :cost, numericality: true
  validates :price_list, presence: true
  validates :stock, numericality: { only_integer: true }
  validates :price_list, uniqueness: { scope: %i[brand code],
                                       case_sensitive: false }
end
