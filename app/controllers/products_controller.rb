class ProductsController < ApplicationController
  before_action :set_product, only: %i[ show edit update destroy ]

  # GET /products or /products.json
  def index
    @products = Product.all.page(params[:page])
  end

  # GET /products/1 or /products/1.json
  def show
  end

  # GET /products/new
  def new
    @product = Product.new
  end

  # GET /products/1/edit
  def edit
  end

  # POST /products or /products.json
  def create
    @product = Product.new(product_params)

    respond_to do |format|
      if @product.save
        format.html { redirect_to @product, notice: "Product was successfully created." }
        format.json { render :show, status: :created, location: @product }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @product.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /products/1 or /products/1.json
  def update
    respond_to do |format|
      if @product.update(product_params)
        format.html { redirect_to @product, notice: "Product was successfully updated." }
        format.json { render :show, status: :ok, location: @product }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @product.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /products/1 or /products/1.json
  def destroy
    @product.destroy
    respond_to do |format|
      format.html { redirect_to products_url, notice: "Product was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  def import_form
  end

  def import
    price_list = params[:price_list].original_filename.gsub(/\.csv$/, '')
    index_title_map = {
      name: %w[Наименование НаименованиеТовара],
      brand: %w[Производитель Бренд],
      code: %w[Артикул Номер],
      stock: %w[Кол-во Количество],
      cost: %w[Цена]
    }
    m = nil
    price_list_product_ids = []
    _options = { quote_char: '"', col_sep: ';', liberal_parsing: true }
    begin
      # retries ||= 0
      CSV.foreach(params[:price_list], headers: true) do |row|
        m ||= set_index(row.to_h, index_title_map)

        p = Product.new(price_list: price_list,
                        name: row[m[:name]],
                        brand: row[m[:brand]].downcase,
                        code: row[m[:code]].downcase,
                        stock: row[m[:stock].gsub('>', '')],
                        cost: row[m[:cost]])

        duplicat = false
        if p.valid?
          # TODO: use Product.import for batches of 1000 products instead
          p.save
        elsif (duplicat_product = Product.find_by(price_list: price_list, brand: p.brand, code: p.code))
          duplicat_product.update(name: p.name, stock: p.stock, cost: p.cost)
          duplicat = true
        else
          # log invalid row if needed
        end

        price_list_product_ids.push(p&.id || duplicat_product.id) if p&.id || duplicat
      end

      # remove products that are not present in an uploaded price list
      # TODO: change to process removal of products in batches of 1000
      Product.where(price_list: price_list)
            .where.not(id: price_list_product_ids).destroy_all
    rescue CSV::MalformedCSVError
      # options[:col_sep] = ';'
      # options[:quote_char] = '"'
      # retry if (retries += 1) < 3
      logger.error 'unhandled CSV exception'
    end

    redirect_to import_products_path
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_product
      @product = Product.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def product_params
      params.require(:product).permit(:price_list, :brand, :code, :stock, :cost, :name)
    end

    def set_index(row, index)
      result = {}
      row.each do |rkey, rval|
        index.each do |key, val|
          if val.include? rkey
            result[key] = rkey
            break
          end
        end
      end
      # byebug
      raise 'could not parse file' if index.size != result.size
      result
    end
end
