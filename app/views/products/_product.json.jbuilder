json.extract! product, :id, :price_list, :brand, :code, :stock, :cost, :name, :created_at, :updated_at
json.url product_url(product, format: :json)
