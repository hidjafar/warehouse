Rails.application.routes.draw do
  resources :products do
    collection do
      get 'import', action: :import_form, as: :import_form
      post 'import', action: :import, as: :import
    end
  end
  root 'products#index'
end
