class CreateProducts < ActiveRecord::Migration[6.1]
  def change
    create_table :products do |t|
      t.string :price_list
      t.string :brand, null: false
      t.string :code, null: false
      t.integer :stock, null: false
      t.decimal :cost, precision: 12, scale: 2, null: false
      t.string :name

      t.timestamps
    end
    add_index :products, %i[price_list brand code], unique: true
  end
end
